from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

FILENAME = "bounce_test"
THRESHOLD = 0

XMIN = 0
XMAX = 329

YMIN = 0
YMAX = 287

ZMIN = 0
ZMAX = 258

def main():

    lsm_xs = []
    lsm_ys = []
    lsm_zs = []

    kalman_xs = []
    kalman_ys = []
    kalman_zs = []

    try:
        lf = open("Trajectory Tests/" + FILENAME + "_lsm.csv")
        count = 0
        for line in lf.readlines():
            try:
                x, y, z = line.split(",")

                if count > THRESHOLD:
                    lsm_xs.append(float(x))
                    lsm_ys.append(float(y))
                    lsm_zs.append(float(z))

                count += 1
            except:
                pass
        lf.close()
    except:
        print("File could not be opened.")

    try:
        kf = open("Trajectory Tests/" + FILENAME + "_kalman.csv")
        count = 0
        for line in kf.readlines():
            try:
                x, y, z = line.split(",")

                if count > THRESHOLD:
                    kalman_xs.append(float(x))
                    kalman_ys.append(float(y))
                    kalman_zs.append(float(z))

                count += 1
            except:
                pass
        kf.close()
    except:
        print("File could not be opened.")

    plt.clf()

    lsm_fig = plt.figure(1)
    lsm_plot = Axes3D(lsm_fig)

    lsm_plot.set_xlim3d(XMIN, XMAX)
    lsm_plot.set_ylim3d(YMIN, YMAX)
    lsm_plot.set_zlim3d(ZMIN, ZMAX)
    lsm_plot.plot(lsm_xs, lsm_ys, lsm_zs, 'b.')

    kalman_fig = plt.figure(2)
    kalman_plot = Axes3D(kalman_fig)

    kalman_plot.set_xlim3d(XMIN, XMAX)
    kalman_plot.set_ylim3d(YMIN, YMAX)
    kalman_plot.set_zlim3d(ZMIN, ZMAX)
    kalman_plot.plot(kalman_xs, kalman_ys, kalman_zs, 'g.')

    plt.show()

if __name__ == "__main__":
    main()
