import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from Smart.Localization.processor import DistanceProcessor
from collections import OrderedDict
from math import sqrt

# Tests the localization module
def get_data(filename):
    xs = []
    ys = []
    zs = []

    f = open(filename, "r")
    for line in f.readlines():
        values = line.split(",")

        xs.append(float(values[0]))
        ys.append(float(values[1]))
        zs.append(float(values[2]))
    f.close()

    return xs, ys, zs

def get_anchor_dicts(filename):
    anchor_dicts = OrderedDict()

    f = open(filename, "r")
    for line in f.readlines():
        values = line.split()

        key = int(values[0])

        coor_dict = OrderedDict()

        coor_dict["x"] = float(values[1])
        coor_dict["y"] = float(values[2])
        coor_dict["z"] = float(values[3])

        anchor_dicts[key] = coor_dict
    f.close()

    return anchor_dicts

def get_distances_dicts():
    xs, ys, zs = get_data("../coordinates.txt")
    anchor_dicts = get_anchor_dicts("Smart/Localization/anchors.txt")

    distance_dicts = []
    for i in range(0, len(xs)):
        distance_dict = OrderedDict()
        for key in anchor_dicts:
            d = sqrt(((anchor_dicts[key]["x"] - xs[i])**2) + ((anchor_dicts[key]["y"] - ys[i])**2) + ((anchor_dicts[key]["z"] - zs[i])**2))
            distance_dict[key] = d
            distance_dicts.append(distance_dict)
    return distance_dicts

def get_ticks(min_val, max_val, step_val):
    i = min_val
    ticks = []
    while i <= max_val:
        ticks.append(i)
        i += step_val
    return ticks

# Plots contents of coordinates.txt and output_coordinates.txt
def test1():
    fig = plt.figure()

    ax = Axes3D(fig)
    obtained_xs, obtained_ys, obtained_zs = get_data("../output_coordinates.txt")
    ax.plot(obtained_xs, obtained_ys, obtained_zs, 'g-')

    true_xs, true_ys, true_zs = get_data("../coordinates.txt")
    ax.plot(true_xs, true_ys, true_zs, 'r-')

    plt.show()

# Uses gets distances from anchors using coordinates.txt. Also uses position getter.
def test2():
    xs = []
    ys = []
    zs = []

    fig = plt.figure()
    ax = Axes3D(fig)

    dp = DistanceProcessor()
    distance_dicts = get_distances_dicts()
    for i in range(0, len(distance_dicts)):
        pos = dp.get_position(distance_dicts[i])
        print("Distance Dictionary: " + str(distance_dicts[i]))
        print("Position: " + str(pos))

        xs.append(pos[0])
        ys.append(pos[1])
        zs.append(pos[2])

        if i == len(distance_dicts) / 2:
            ax.plot(xs = xs, ys = ys, zs = zs)
            plt.show()

    ax.plot(xs = xs, ys = ys, zs = zs)
    plt.show()

def main():
    print("Running first test")
    test1()

    print("")
    print("Running second test")
    test2()

if __name__ == "__main__":
    main()
