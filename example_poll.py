import msvcrt

from Smart.controller import Controller, ENABLE_GESTURE, ENABLE_LOCALIZATION, ENABLE_ALL, MODE_POLL

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

def main():
    controller = Controller(ENABLE_GESTURE, MODE_POLL)
    controller.start()
    print("Press CTRL + ALT + Q to exit.")
    while True:
        try:
            x = kbfunc()

            gesture = controller.get_gesture()
            if gesture != None:
                print("Gesture: " + str(gesture))
            if x == 16:
                raise KeyboardInterrupt
            elif not(controller.errors.empty()):
                print("One or more errors have occured.")
                while not(controller.errors.empty()):
                    print("\t" + str(controller.errors.get()))
                raise Exception("An error has occurred.")
        except:
            controller.stop()
            break

if __name__ == "__main__":
    main()
