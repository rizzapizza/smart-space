import numpy as np

def get_corrcoef(sample_set, start, end):
    X = sample_set.get_subarray(start, end, 'x')
    Y = sample_set.get_subarray(start, end, 'y')

    corr_matrix = np.corrcoef(X, Y)
    return corr_matrix[0][1]
