from Smart.kalman_state import KalmanState, Q, R, P
from Smart.Gesture.offset_remover import OffsetRemover

from Smart.Gesture.types import AccelerationSample, AccelerationSampleSet

from Smart.Gesture.acceleration_vector_creator import AccelerationVectorCreator
from Smart.Gesture.gesture_recognizer import GestureRecognizer

from Smart.csv_writer import CSVWriter

ACCELERATION_DUE_TO_GRAVITY = 9.8

class AccelerationProcessor(object):
    def __init__(self, window_size, max_sample_len, filename = None):
        self.x_offset_remover = OffsetRemover(window_size)
        self.y_offset_remover = OffsetRemover(window_size)

        self.x_state = KalmanState(q = Q, r = R, p = P, initial_val = 0)
        self.y_state = KalmanState(q = Q, r = R, p = P, initial_val = 0)

        # for orientation (also Kalman filter)
        self.x_orientation_state = KalmanState(q = Q, r = R, p = P, initial_val = 0)
        self.y_orientation_state = KalmanState(q = Q, r = R, p = P, initial_val = ACCELERATION_DUE_TO_GRAVITY)

        self.sample_set = AccelerationSampleSet(max_sample_len)
        self.vector_creator = AccelerationVectorCreator(self.sample_set)
        self.recognizer = GestureRecognizer()

        if filename != None:
            self.write_to_file = True

            self.x_writer = CSVWriter("AccelerationData/x_" + filename + ".csv")
            self.y_writer = CSVWriter("AccelerationData/y_" + filename + ".csv")

            header = ["Time (ms)", "Raw Data", "Data with Removed Offset", "Filtered Data", "Orientation"]

            self.x_writer.writeRow(header)
            self.y_writer.writeRow(header)
        else:
            self.write_to_file = False

    def reset(self):
        self.sample_set.clear()

        self.x_offset_remover.reset()
        self.y_offset_remover.reset()

    def get_gesture(self, data):
        ms = int(data[0])

        raw_x = data[1]
        raw_y = data[2]

        self.x_orientation_state.update(measurement = raw_x)
        self.y_orientation_state.update(measurement = raw_y)

        # get values with removed offset
        norm_x = self.x_offset_remover.getOffsetRemovedData(raw_x)
        norm_y = self.y_offset_remover.getOffsetRemovedData(raw_y)

        # update Kalman states
        self.x_state.update(measurement = norm_x)
        self.y_state.update(measurement = norm_y)

        if self.write_to_file:
            self.x_writer.writeRow([ms, raw_x, norm_x, self.x_state.filtered_val, self.x_orientation_state.filtered_val])
            self.y_writer.writeRow([ms, raw_y, norm_y, self.y_state.filtered_val, self.y_orientation_state.filtered_val])

        # print("Acceleration at time %d: %f, %f" % (ms, self.x_state.filtered_val, self.y_state.filtered_val))

        sample = AccelerationSample(self.x_state.filtered_val, self.y_state.filtered_val, ms, self.x_orientation_state.filtered_val, self.y_orientation_state.filtered_val)
        self.sample_set.add(sample)
        if self.sample_set.is_full():
            vector = self.vector_creator.get_vector()
            gesture = self.recognizer.recognize(vector)
            if gesture != None and len(self.vector_creator.peak_indices) > 0:
                self.sample_set.shorten(start = self.vector_creator.peak_indices[0])
            return gesture

        return None

    def close(self):
        if self.write_to_file:
            self.x_writer.close()
            self.y_writer.close()
