from __future__ import division, print_function

from Smart.Gesture.types import XYTuple
from Smart.Gesture.types import TimeAxisTuple
from Smart.Gesture.detect_peaks import detect_peaks

import numpy as np
import matplotlib.pyplot as plt

MAX_PEAK_COUNT = 2
MPH = 0.2
THRESHOLD = 0.001

class PeakFinder(object):
    def __init__(self):
        self.time_axis_tuples = []
        self.curr_time_axis_tuple = None

    def mark_current_peakset_inspected(self):
        self.time_axis_tuples.append(self.curr_time_axis_tuple)
        self.curr_time_axis_tuple = None

    def find_peaks(self, sample_set):
        data = XYTuple([], [])
        time = []

        for i in range(0, len(sample_set)): # get data from sample set into lists
            data.x.append(sample_set[i].x)
            data.y.append(sample_set[i].y)
            time.append(sample_set[i].t)

        return self.__find(time, data)

    def __find(self, time, data):
        peak_indices = XYTuple()

        peak_indices.x = self.__combine_peaks_and_valleys(self.__get_peaks(data.x), self.__get_valleys(data.x))
        peak_indices.y = self.__combine_peaks_and_valleys(self.__get_peaks(data.y), self.__get_valleys(data.y))

        take_x_peaks = False
        take_y_peaks = False

        if self.__has_alternating_signs(data.x, peak_indices.x):
            if self.__has_alternating_signs(data.y, peak_indices.y):
                if peak_indices.x[0] < peak_indices.y[0]:
                    self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.x[0]], 'x')
                    if self.curr_time_axis_tuple in self.time_axis_tuples:
                        self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.y[0]], 'y')
                        if not(self.curr_time_axis_tuple in self.time_axis_tuples):
                            take_y_peaks = True
                    else:
                        take_x_peaks = True
                elif peak_indices.x[0] > peak_indices.y[0]:
                    self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.y[0]], 'y')
                    if self.curr_time_axis_tuple in self.time_axis_tuples:
                        self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.x[0]], 'x')
                        if not(self.curr_time_axis_tuple in self.time_axis_tuples):
                            take_x_peaks = True
                    else:
                        take_y_peaks = True
                else:
                    if abs(data.x[peak_indices.x[0]]) < abs(data.y[peak_indices.y[0]]):
                        self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.y[0]], 'y')
                        if self.curr_time_axis_tuple in self.time_axis_tuples:
                            self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.x[0]], 'x')
                            if not(self.curr_time_axis_tuple in self.time_axis_tuples):
                                take_x_peaks = True
                        else:
                            take_y_peaks = True
                    elif abs(data.x[peak_indices.x[0]]) < abs(data.y[peak_indices.y[0]]):
                        self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.x[0]], 'x')
                        if self.curr_time_axis_tuple in self.time_axis_tuples:
                            self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.y[0]], 'y')
                            if not(self.curr_time_axis_tuple in self.time_axis_tuples):
                                take_y_peaks = True
                        else:
                            take_x_peaks = True
            else:
                self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.x[0]], 'x')
                if not(self.curr_time_axis_tuple in self.time_axis_tuples):
                    take_x_peaks = True
        else:
            if self.__has_alternating_signs(data.y, peak_indices.y):
                self.curr_time_axis_tuple = TimeAxisTuple(time[peak_indices.y[0]], 'y')
                if not(self.curr_time_axis_tuple in self.time_axis_tuples):
                    take_y_peaks = True

        if not(take_x_peaks) and not(take_y_peaks):
            return None
        else:
            if take_x_peaks:
                return ['x'] + peak_indices.x[0:MAX_PEAK_COUNT]
            else:
                return ['y'] + peak_indices.y[0:MAX_PEAK_COUNT]

    def __has_alternating_signs(self, data, indices):
        if self.__has_sufficient_length(indices):
            for i in range(1, MAX_PEAK_COUNT):
                if data[indices[i - 1]] * data[indices[i]] >= 0:
                    return False
            return True
        return False

    def __has_sufficient_length(self, indices):
        return (indices != None and len(indices) >= MAX_PEAK_COUNT)

    def __get_peaks(self, data):
        peaks = detect_peaks(data, mpd = 1, mph = MPH, threshold = THRESHOLD, edge = 'rising')
        return peaks

    def __get_valleys(self, data):
        valleys = detect_peaks(data, mpd = 1, mph = MPH, threshold = THRESHOLD, valley = True, edge = 'rising')
        return valleys

    def __combine_peaks_and_valleys(self, peak_indices, valley_indices):
        i = 0
        j = 0

        peak_indices.sort()
        valley_indices.sort()

        result = []
        while i < len(peak_indices) or j < len(valley_indices):
            if i == len(peak_indices):
                result.append(valley_indices[j])
                j += 1
            elif j ==  len(valley_indices):
                result.append(peak_indices[i])
                i += 1
            else:
                if peak_indices[i] < valley_indices[j]:
                    result.append(peak_indices[i])
                    i += 1
                else: # is not possible for peak_indices[i] and arr[j] to be equal.
                    result.append(valley_indices[j])
                    j += 1
        return result
