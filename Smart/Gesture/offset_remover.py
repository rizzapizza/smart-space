class OffsetRemover(object):
    def __init__(self, window_size):
        self.window_size = window_size
        self.dataset = []

    def addToDataSet(self, data):
        if len(self.dataset) == self.window_size:
            self.dataset.pop(0)
        self.dataset.append(data)

    def getOffsetRemovedData(self, data):
        if self.dataset:
            if len(self.dataset) < self.window_size:
                max_len = len(self.dataset)
            else:
                max_len = self.window_size

            mean = self.getMeanOfSubarray(self.dataset, len(self.dataset) - max_len, len(self.dataset))
            self.addToDataSet(data)
            return (data - mean)
        else:
            self.addToDataSet(data)
            return data

    def getMeanOfSubarray(self, dataset, start, stop):
        return sum(dataset[start:stop]) / (stop - start)

    def reset(self):
        self.dataset = []
