import numpy as np

def __get_data(sample_set, axis, start, end):
    return sample_set.get_subarray(start, end, axis)

def __get_sum(data):
    return np.sum(data)

def __get_fft(data):
    return np.fft.fft(data)

def __get_spectrum(sample_set, axis, start, end):
    return abs(__get_fft(__get_data(sample_set, axis, start, end)))

def compute_energy(sample_set, axis, start, end):
    return __get_sum((__get_spectrum(sample_set, axis, start, end))**2)

def get_energy_of_both_axes(sample_set, start, end):
    return compute_energy(sample_set, 'x', start, end), compute_energy(sample_set, 'y', start, end)
