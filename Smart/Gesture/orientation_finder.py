from Smart.Gesture.types import Orientation

def find_left_orientation(sample_set, init_index):
    i = init_index
    init_time = sample_set[i].t

    orientation = Orientation(0, 0)
    while i >= 0 and (init_time - sample_set[i].t) < 200:
        orientation.x += sample_set[i].orientation.x
        orientation.y += sample_set[i].orientation.y
        i -= 1

    orientation.x /= (init_index - i + 1)
    orientation.y /= (init_index - i + 1)

    return orientation

def find_right_orientation(sample_set, init_index):
    i = init_index
    init_time = sample_set[i].t

    orientation = Orientation(0, 0)
    while i < len(sample_set) and (init_time - sample_set[i].t) < 200:
        orientation.x += sample_set[i].orientation.x
        orientation.y += sample_set[i].orientation.y
        i += 1

    orientation.x /= (i - init_index + 1)
    orientation.y /= (i - init_index + 1)

    return orientation
