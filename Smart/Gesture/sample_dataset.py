import os
import csv

from Smart.Gesture.types import AccelerationFeatureVector

class AccelerationSampleDataset(object):
    def __init__(self, rootdir):
        self.data = []
        self.target = []

        for subdir, dirs, files in os.walk(rootdir):
            for f in files:
                filepath = subdir + os.sep + f

                if filepath.endswith(".csv"):
                    fd = open(filepath)
                    rows = csv.DictReader(fd)

                    for row in rows:
                        vector = AccelerationFeatureVector()
                        for key, value in row.iteritems():
                            if key == "Class":
                                target = value
                            elif key != "Gesture Name":
                                vector.set_value_of_key(value, key)

                        self.data.append(vector.to_list())
                        self.target.append(target)

                    fd.close()
