def __find_zero_crossing_time(sample_set, index, axis):
    t = (0 - sample_set[index].get(axis)) * ((sample_set[index + 1].t - sample_set[index].t)/(sample_set[index + 1].get(axis) - sample_set[index].get(axis))) + sample_set[index].t
    return t

def find_single_duration(sample_set, left_index, right_index):
    if not(sample_set[right_index].t) or not(sample_set[left_index].t):
        return None
    else:
        return (sample_set[right_index].t - sample_set[left_index].t)

def find_multiple_durations(sample_set, indices, axis):
    indices.sort()
    result = []
    for i in range(1, len(indices) - 1):
        if i % 2 == 1:
            zero_crossing_time = __find_zero_crossing_time(sample_set, indices[i], axis)
            result.append(zero_crossing_time - sample_set[indices[i - 1]].t)
            result.append(sample_set[indices[i + 1]].t - zero_crossing_time)
    return result
