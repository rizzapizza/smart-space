from collections import OrderedDict

class Gesture:
    Left = "Left"
    Right = "Right"
    Up = "Up"
    Down = "Down"

class XYTuple(object):
    def __init__(self, x = None, y = None):
        self.x = x
        self.y = y

    def get(self, axis):
        if axis == 'x':
            return self.x
        elif axis == 'y':
            return self.y
        else:
            raise Exception("Error: no such axis exists.")

    def put(self, axis, data):
        if axis == 'x':
            self.x = data
        elif axis == 'y':
            self.y = data
        else:
            raise Exception("Error: no such axis exists.")

class Orientation(object):
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __eq__(self, orientation):
        return (self.x == orientation.x and self.y == orientation.y)

    def __str__(self):
        return str("(") + str(self.x) + ", " + str(self.y) + str(")")

class AccelerationSample(object):
    def __init__(self, x, y, t, orientation_x, orientation_y):
        self.x = float(x)
        self.y = float(y)
        self.t = float(t)

        self.orientation = Orientation(orientation_x, orientation_y)

    def get(self, axis):
        if axis == 'x':
            return self.x
        elif axis == 'y':
            return self.y
        else:
            raise Exception("Error: no such axis exists.")

    def __eq__(self, sample):
        return (self.x == sample.x and self.y == sample.y and self.t == sample.t and self.orientation == sample.orientation)

    def __str__(self):
        return str(self.t) + ": (" + str(self.x) + "," + str(self.y) + ")\n" + str(self.orientation)

class AccelerationSampleSet(object):
    def __init__(self, max_len):
        self._max_len = max_len
        self.samples = []

        self._last_ms = 0 # Initilizes the last ms entry
        self._samples_since_overflow = 0
        self._overflow = False # An overflow flag
        self._max_power_2 = 0

    def is_full(self):
        return (len(self.samples) == self._max_len)

    def is_empty(self):
        return (len(self.samples) == 0)

    def add(self, item):
        if self._last_ms > item.t or self._overflow: # Overflow has occurred. Find the nearest power of two greater than last ms. Call this self._max_power_2.
            if not(self._overflow): # Overflow flag has not been set yet
                try:
                    # print("Last ms: " + str(self._last_ms))
                    self._max_power_2 = pow(self._last_ms, 0.5) # the square root of last_ms
                    self._max_power_2 = pow(2, self._max_power_2 + 1)

                    item.t = item.t + self._max_power_2 # item.t = last ms + (m - last_ms) + item.t = m + item.t

                    self._samples_since_overflow = 0
                    self._overflow = True
                except OverflowError:
                    print("Overflow error")
                    pass
            else:
                item.t = item.t + self._max_power_2 # item.t = last ms + (m - last_ms) + item.t = m + item.t
                if self._samples_since_overflow < self._max_len:
                    self._samples_since_overflow += 1
                else: # The last number before overflow has been removed from the sample set
                    self._overflow = False
                    for i in range(0, len(self.samples)):
                        self.samples[i].t = self.samples[i].t - self._max_power_2
        elif self._last_ms < item.t:
            # Set last ms to current ms for the next round
            # print("Item.t = " + str(item.t))
            self._last_ms = item.t

            if self.is_full():
                self.samples.pop(0)
            self.samples.append(item)
        else:
            print("Last time and current time are equal. Sample not included in set.")

    def clear(self):
        self.samples = []

    def __is_valid_index(self, index, max_len):
        return (index > 0 and index < max_len)

    def __is_valid_start_end(self, start, end):
        return (end > start)

    def shorten(self, start = None, end = None):
        if start != None:
            if self.__is_valid_index(start, len(self)):
                if end != None:
                    if self.__is_valid_index(end, len(self)):
                        if self.__is_valid_start_end(start, end):
                            self.samples = self.samples[start:end]
                        else:
                            raise Exception("Error: Start and end must specify a subarray with length less than or equal to the new maximum length")
                    else:
                        raise Exception("Error: End index is out of range.")
                else:
                    self.samples = self.samples[start:]
            else:
                raise Exception("Error: Start index is out of range.")
        else:
            if end != None:
                if self.__is_valid_index(end, len(self)):
                    self.samples = self.samples[:end]
                else:
                    raise Exception("Error: End index is out of range.")
            else:
                raise Exception("Error: Start and end cannot both be none.")

    def get_subarray(self, start, end, axis):
        subarray = []
        for i in range(start, end):
            subarray.append(self.samples[i].get(axis))
        return subarray

    def replace(self, item):
        self.clear()
        self.add(item)

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, index):
        return self.samples[index]

class TimeAxisTuple(object):
    def __init__(self, time, axis):
        self.time = time
        self.axis = axis

    def __eq__(self, time_axis_tuple):
        return (self.time == time_axis_tuple.time and self.axis == time_axis_tuple.axis)

    def __str__(self):
        return str(self.time) + " " + str(self.axis)

class AccelerationFeatureVector(object):
    def __init__(self, axis = None, left_peak = None, right_peak = None, whole_duration = None, peak_duration = None, corrcoef = None, energy_ratio = None, left_mean = None, right_mean = None, left_orientation = None, right_orientation = None):
        self.dict = OrderedDict()

        self.set_axis(axis)

        if axis != None:
            self.dict["Left Peak"] = left_peak.get(axis)
            self.dict["Right Peak"] = right_peak.get(axis)
        else:
            self.dict["Left Peak"] = None
            self.dict["Right Peak"] = None

        self.dict["Whole Duration"] = whole_duration
        self.dict["Peak Duration"] = peak_duration

        self.dict["Correlation Coefficient"] = corrcoef
        self.dict["Energy Ratio"] = energy_ratio

        self.dict["Left Mean"] = left_mean
        self.dict["Right Mean"] = right_mean

        try:
            self.dict["Left Orientation (X)"] = left_orientation.x
            self.dict["Right Orientation (X)"] = right_orientation.x
        except:
            self.dict["Left Orientation (X)"] = None
            self.dict["Right Orientation (X)"] = None

        try:
            self.dict["Left Orientation (Y)"] = left_orientation.y
            self.dict["Right Orientation (Y)"] = right_orientation.y
        except:
            self.dict["Left Orientation (Y)"] = None
            self.dict["Right Orientation (Y)"] = None

    def to_list(self):
        result = []
        for key, value in self.dict.iteritems():
            result.append(value)
        return result

    def set_axis(self, axis):
        if axis == 'x':
            self.dict["Axis"] = 0
        elif axis == 'y':
            self.dict["Axis"] = 1
        elif axis == 0 or axis == 1:
            self.dict["Axis"] = axis
        else:
            self.dict["Axis"] = None

    def get_axis(self):
        if self.dict["Axis"] == 0:
            return 'x'
        elif self.dict["Axis"] == 1:
            return 'y'
        else:
            return None

    def get_left_peak(self):
        return self.dict["Left Peak"]

    def set_value_of_key(self, input_value, input_key):
        found_key = False
        for key in self.dict:
            if key == input_key:
                if key == "Axis":
                    self.set_axis(int(input_value))
                else:
                    try:
                        self.dict[key] = float(input_value)
                    except:
                        self.dict[key] = input_value
                found_key = True

        if not(found_key):
            raise Exception("Error: key not found.")
