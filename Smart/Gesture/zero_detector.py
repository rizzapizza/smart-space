import matplotlib.pyplot as plt

def __get_slope(sample1, sample2, axis):
    return (sample2.get(axis) - sample1.get(axis)) / (sample2.t - sample1.t)

def find_left_edge_zero_index(sample_set, axis, peak_index):
    min_abs_slope = None
    prev_slope = None

    i = peak_index - 1
    while i >= 0:
        curr_slope = __get_slope(sample_set[i + 1], sample_set[i], axis)
        if min_abs_slope == None:
            min_abs_slope = abs(__get_slope(sample_set[i + 1], sample_set[i], axis))
        else:
            if abs(curr_slope) <= min_abs_slope: # if slope is flat
                return i
            elif (sample_set[i + 1].get(axis) * sample_set[i].get(axis)) < 0: # if went to a zero-crossing
                return i
            elif (prev_slope * curr_slope) < 0: # if slope sign changes
                return i
        prev_slope = curr_slope
        i -= 1

    return 0

def find_middle_zero_index(sample_set, axis, left_peak_index, right_peak_index):
    for i in range(left_peak_index, right_peak_index + 1):
        if (sample_set[i].get(axis) * sample_set[i + 1].get(axis)) < 0:
            return i
    return None

def find_right_edge_zero_index(sample_set, axis, peak_index):
    i = peak_index + 1
    while i < len(sample_set):
        if (sample_set[i - 1].get(axis) * sample_set[i].get(axis)) < 0: # if went to a zero-crossing
            return i
        i += 1

    return None

def find_zeros(sample_set, axis, peak_indices):
    left_edge_zero_index = find_left_edge_zero_index(sample_set, axis, peak_indices[0])
    right_edge_zero_index = find_right_edge_zero_index(sample_set, axis, peak_indices[-1])

    return [left_edge_zero_index, right_edge_zero_index]
