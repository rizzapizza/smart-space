def find_left_mean(sample_set, axis, init_index):
    i = init_index
    init_time = sample_set[i].t

    values_sum = 0
    while i >= 0 and (init_time - sample_set[i].t) < 200:
        values_sum += sample_set[i].get(axis)
        i -= 1

    return (values_sum / (init_index - i + 1))

def find_right_mean(sample_set, axis, init_index):
    i = init_index
    init_time = sample_set[i].t

    values_sum = 0
    while i < len(sample_set) and (init_time - sample_set[i].t) < 200:
        values_sum += sample_set[i].get(axis)
        i += 1

    return (values_sum / (i - init_index + 1))
