from Smart.Gesture.types import Gesture

from sklearn import svm
from Smart.Gesture.sample_dataset import AccelerationSampleDataset

class GestureRecognizer(object):
    def __init__(self):
        self.clf = svm.SVC(gamma = 0.001, C = 100)
        dataset = AccelerationSampleDataset(rootdir = "Smart\Gesture\classified")
        self.clf.fit(dataset.data, dataset.target)

    def recognize(self, vector):
        if vector != None:
            prediction = str(self.clf.predict(vector.to_list()))
            if prediction[2:len(prediction) - 2] == "Non-gesture":
                return None
            else:
                if vector.get_axis() == 'x':
                    if vector.get_left_peak() < 0:
                        return Gesture.Left
                    else:
                        return Gesture.Right
                elif vector.get_axis() == 'y':
                    if vector.get_left_peak() < 0:
                        return Gesture.Down
                    else:
                        return Gesture.Up
                else:
                    return None
        else:
            return None
