from Smart.Gesture.types import XYTuple
from Smart.Gesture.types import AccelerationFeatureVector

from Smart.Gesture.peak_finder import PeakFinder

import Smart.Gesture.duration_computer as dc
import Smart.Gesture.mean_computer as mc
import Smart.Gesture.orientation_finder as of
import Smart.Gesture.zero_detector as zd
import Smart.Gesture.correlation_getter as cg
import Smart.Gesture.energy_computer as ec

class AccelerationVectorCreator(object):
    def __init__(self, sample_set):
        self.sample_set = sample_set

        # stores most recently computed parameters
        self.axis = None
        self.peak_indices = []
        self.zero_indices = []

        self.peak_finder = PeakFinder()

    def __valid_data(self, data, proper_len):
        if data == None:
            return False
        else:
            if len(data) != proper_len:
                return False
            else:
                for i in range(0, len(data)):
                    if data[i] == None:
                        return False
                return True

    def get_vector(self, should_plot = False, debugging = False, by_point = False): # called when sample set is full
        res = self.peak_finder.find_peaks(self.sample_set)
        if debugging:
            print("Peak results: " + str(res))
        if res != None:
            self.axis = res[0]
            self.peak_indices = res[1:]
            if debugging:
                print("Axis: " + str(self.axis))
                print("Peak indices: " + str(self.peak_indices))

            if self.__valid_data([self.axis] + self.peak_indices, 3):
                if debugging:
                    print("Successfully obtained axis and peak indices from peak finder.")

                self.zero_indices = zd.find_zeros(self.sample_set, self.axis, self.peak_indices)
                if debugging:
                    print("Zero indices: " + str(self.zero_indices))

                if self.__valid_data(self.zero_indices, 2):
                    if debugging:
                        print("Successfully obtained zero indices.")

                    self.peak_finder.mark_current_peakset_inspected()

                    whole_duration = dc.find_single_duration(self.sample_set, self.zero_indices[0], self.zero_indices[-1])
                    peak_duration = dc.find_single_duration(self.sample_set, self.peak_indices[0], self.peak_indices[-1])
                    if debugging:
                        print("Whole duration: " + str(whole_duration))
                        print("Peak duration: " + str(peak_duration))

                    corrcoef = cg.get_corrcoef(self.sample_set, self.zero_indices[0], (self.zero_indices[-1] + 1))
                    if debugging:
                        print("Correlation Coefficient: " + str(corrcoef))

                    x_energy, y_energy = ec.get_energy_of_both_axes(self.sample_set, self.zero_indices[0], (self.zero_indices[-1] + 1))
                    if self.axis == 'x':
                        energy_ratio = x_energy / y_energy
                    else:
                        energy_ratio = y_energy / x_energy
                    if debugging:
                        print("Energy Ratio: " + str(energy_ratio))

                    left_mean = mc.find_left_mean(self.sample_set, self.axis, self.zero_indices[0])
                    right_mean = mc.find_right_mean(self.sample_set, self.axis, self.zero_indices[-1])
                    if debugging:
                        print("Left mean: " + str(left_mean))
                        print("Right mean: " + str(right_mean))
                        print("Successfully obtained left and right mean values.")

                    left_orientation = of.find_left_orientation(self.sample_set, self.zero_indices[0])
                    right_orientation = of.find_right_orientation(self.sample_set, self.zero_indices[-1])
                    if debugging:
                        print("Left orientation: " + str(left_orientation))
                        print("Right orientation: " + str(right_orientation))
                        print("Successfully obtained left and right orientations.")

                    left_peak = XYTuple(self.sample_set[self.peak_indices[0]].x, self.sample_set[self.peak_indices[0]].y)
                    right_peak = XYTuple(self.sample_set[self.peak_indices[-1]].x, self.sample_set[self.peak_indices[-1]].y)

                    vector = AccelerationFeatureVector(self.axis, left_peak, right_peak, whole_duration, peak_duration, corrcoef, energy_ratio, left_mean, right_mean, left_orientation, right_orientation)
                    return vector
        return None
