from threading import Thread
from Queue import Queue

from Smart.state import ProcessState
from Smart.Gesture.processor import AccelerationProcessor
from Smart.Localization.processor import DistanceProcessor
from Smart.errors import GATEWAY_NOT_FOUND, INVALID_CONTROLLER_INIT
from Smart.csv_writer import CSVWriter

from serial import Serial
from serial.tools import list_ports

from collections import OrderedDict

WINDOW_SIZE = 10
MAX_SAMPLE_LEN = 36

NO_OF_PROCESSES = 2

ENABLE_GESTURE = 10
ENABLE_LOCALIZATION = 11
ENABLE_ALL = 12

MODE_POLL = 13
MODE_LISTEN = 14

GET_LSM = 15
GET_KALMAN = 16
GET_BOTH = 17

MAX_TRIALS = 2

DEFAULT_Z_OFFSET = 258 # Because anchor nodes are on the ceiling

class SerialReader(object): # throws exception
    def __init__(self, errors, verbose = False):
        self.serial = None
        self.errors = errors
        self.verbose = verbose
        if self.verbose:
            print("self.serial = None")

        ports = list(list_ports.comports())
        if self.verbose:
            print("ports = list(list_ports.comports())")
        for p in ports:
            print("Found " + str(p[0]))
            try:
                if self.verbose:
                    print("Arduino serial connection found at " + str(p[0]))
                self.serial = Serial(str(p[0]), 9600, timeout = 1) # Every time this piece of code is ran, Arduino resets
            except:
                continue

            print("Initializing serial port.")
            if self.init_nodes():
                print("Succesfully initialized gateway.")
                break
            else:
                print("Wrong serial port")
                self.serial.close()
                self.serial = None

        if self.serial == None:
            self.errors.put(GATEWAY_NOT_FOUND)

    def init_nodes(self):
        mobile_working = False
        anchor_count = 0

        failure_count = 0
        while True:
            line = self.serial.readline()
            values = line.split()

            if len(values) > 0:
                if values[0] == "M":
                    mobile_working = True
                    print("Mobile is working")
                elif values[0] == "N":
                    try:
                        node = int(values[1])
                        print("Node " + str(node) + " is working.")
                        anchor_count += 1
                    except:
                        self.errors.put("Invalid data format during initialization.")
                elif values[0] == ";":
                    if anchor_count >= 3 and mobile_working:
                        return True
                    else:
                        self.errors.put("One or more of the anchor and mobile nodes is not working.")
                elif values[0] == "E":
                    self.errors.put(line + " Please try pressing the reset button on the mobile device and then restarting this program.")
                elif values[0] == "A" and len(values) == 4: # Since Arduino resets after a serial connection is opened, the following case need not be considered.
                    self.errors.put("Warning: The gateway device did not give sufficient initialization information. System software will assume that device has been properly initialized already.")
                    return True
                elif values[0] == "L" and len(values) >= 7: # Since Arduino resets after a serial connection is opened, the following case need not be considered.
                    self.errors.put("Warning: The gateway device did not give sufficient initialization information. System software will assume that device has been properly initialized already.")
                    return True
                else:
                    if failure_count < MAX_TRIALS:
                        failure_count += 1
                    else:
                        return False
            else:
                if failure_count < MAX_TRIALS:
                    failure_count += 1
                else:
                    return False

    def parseline(self):
        line = self.serial.readline()
        if self.verbose:
            print("Line inside parseline() function: " + str(line))
        values = line.split()

        if len(values) > 0:
            if values[0] == "A" and len(values) == 4: # Ex. A 50 0 9.8 -> 50 here is time in milliseconds
                if self.verbose:
                    print("Acceleration data is being read.")
                try:
                    results = []
                    for i in range(1, len(values)):
                        results.append(float(values[i]))
                    return ProcessState.GESTURE, results
                except:
                    if self.verbose:
                        print("An exception occurred.")
                    return None
            elif values[0] == "L" and len(values) >= 7:
                if self.verbose:
                    print("Localization data is being read.")
                try:
                    results = OrderedDict()
                    i = 1
                    while i < (len(values) - 1):
                        key = int(values[i]) # anchor number
                        value = float(values[i + 1]) # distance of mobile to anchor

                        results[key] = value
                        i += 2
                    return ProcessState.LOCALIZATION, results
                except:
                    if self.verbose:
                        print("An exception occurred.")
                    return None
            elif values[0] == "E":
                self.errors.put(line)
            else:
                return None
        else:
            return None

    def flush(self):
        self.serial.flushInput()

    def close(self):
        self.serial.close()

class Controller(object):
    def __init__(self, enable, mode, listen_for_localization_none = False, listen_for_gesture_none = False, verbose = False, acceleration_filename = None, localization_mode = GET_KALMAN, include_distance = False):
        self.gesture_enabled = False
        self.localization_enabled = False

        self.listeners = []

        if enable == ENABLE_GESTURE:
            self.gesture_enabled = True
        elif enable == ENABLE_LOCALIZATION:
            self.localization_enabled = True
        elif enable == ENABLE_ALL:
            self.gesture_enabled = True
            self.localization_enabled = True
        else:
            self.errors.put(INVALID_CONTROLLER_INIT)

        self.errors = Queue()

        self.gestures = Queue()
        self.locations = Queue()

        self.data = Queue()
        self.states = Queue()
        self.stopper = Queue(NO_OF_PROCESSES)

        self.reader = Thread(target = self.read, args = ())
        self.processor = Thread(target = self.process, args = ())

        self.mode = mode
        self.listen_for_localization_none = listen_for_localization_none
        self.listen_for_gesture_none = listen_for_gesture_none # This option only matters when the controller is operating in the listener mode. If this is true, then none gestures trigger the calling of listener on_gesture functions. If not, on_gesture is called only when detected gesture is not none.

        self.init_done = Queue(1)

        self.verbose = verbose
        self.acceleration_filename = acceleration_filename

        self.localization_mode = localization_mode
        self.include_distance = include_distance

    def start(self):
        print("Processes begun.")
        self.reader.start()
        self.processor.start()

    def get_gesture(self):
        if self.gesture_enabled:
            while self.gestures.empty():
                pass
            return self.gestures.get()
        return None

    def get_location(self):
        if self.localization_enabled:
            while self.locations.empty():
                pass
            return self.locations.get()

    def stop(self):
        if self.stopper.empty():
            print("Processes stopped.")
            for i in range(0, NO_OF_PROCESSES):
                if not(self.stopper.full()):
                    self.stopper.put('DONE')
                else:
                    break

            self.reader.join()
            self.processor.join()

    def read(self):
        serial_reader = SerialReader(self.errors, self.verbose)
        self.init_done.put(True)
        if serial_reader.serial != None:
            serial_reader.flush()

            while self.stopper.empty():
                line = serial_reader.parseline()
                if self.verbose:
                    print("Line: " + str(line))
                if line != None:
                    state = line[0]
                    parsed = line[1]
                    if state == ProcessState.GESTURE and self.gesture_enabled:
                        self.states.put(state)
                        self.data.put(parsed)
                        
                        if self.mode == MODE_LISTEN:
                            for i in range(0, len(self.listeners)):
                                self.listeners[i].on_acceleration(parsed) # pass acceleration data to listeners
                    elif state == ProcessState.LOCALIZATION and self.localization_enabled:
                        self.states.put(state)
                        self.data.put(parsed)

            serial_reader.close()

    def process(self):
        ap = AccelerationProcessor(WINDOW_SIZE, MAX_SAMPLE_LEN, self.acceleration_filename)
        dp = DistanceProcessor(errors = self.errors, z_offset = DEFAULT_Z_OFFSET, localization_mode = self.localization_mode, include_distance = self.include_distance)

        prev_state = None

        while self.stopper.empty():
            if self.verbose:
                print("Processing")
            if not(self.data.empty()):
                state = self.states.get()
                parsed = self.data.get()
                if state == ProcessState.GESTURE:
                    if state != prev_state:
                        ap.reset()

                    gesture = ap.get_gesture(parsed)
                    if self.mode == MODE_POLL:
                        self.gestures.put(gesture)
                    elif self.mode == MODE_LISTEN:
                        if gesture == None:
                            if self.listen_for_gesture_none:
                                self.call_on_gesture(gesture)
                        else:
                            self.call_on_gesture(gesture)
                else:
                    if state != prev_state:
                        dp.reset()

                    print(parsed)
                    location = dp.get_position(parsed)
                    if self.mode == MODE_POLL:
                        self.locations.put(location)
                    elif self.mode == MODE_LISTEN:
                        if location == None:
                            if self.listen_for_localization_none:
                                self.call_on_location(location)
                        else:
                            self.call_on_location(location)
                prev_state = state

        ap.close()

    def add_listener(self, listener):
        print("Added a new listener.")
        self.listeners.append(listener)

    def remove_last_listener(self):
        if len(self.listeners) > 0:
            self.listeners.pop(len(self.listeners) - 1)

    def call_on_gesture(self, gesture):
        for i in range(0, len(self.listeners)):
            self.listeners[i].on_gesture(gesture)

    def call_on_location(self, location):
        for i in range(0, len(self.listeners)):
            self.listeners[i].on_location(location)
