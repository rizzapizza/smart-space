Q = 0.0625
R = 32.0
P = 1.3833094

class KalmanState:
	def __init__(self, q, r, p, initial_val):
		self.q = float(q) # process noise covariance
		self.r = float(r) # measurement noise covariance
		self.p = float(p) # estimation error covariance
		self.k = 1
		self.filtered_val = float(initial_val)

	def update(self, measurement):
		# prediction update
		# self.filtered_val = self.filtered_val
		self.p = self.p + self.q

		# measurement update
		self.k = float(self.p / (self.p + self.r))
		self.filtered_val = self.filtered_val + self.k * float(measurement - self.filtered_val)
		self.p = (1 - self.k) * self.p
