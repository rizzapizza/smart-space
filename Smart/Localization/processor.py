from Smart.kalman_state import KalmanState

from Smart.Localization.types import AnchorNodes
from Smart.Localization.position_getter import PositionGetter
# from Smart.errors import INSUFFICIENT_DISTANCES

GET_LSM = 15
GET_KALMAN = 16
GET_BOTH = 17

class DistanceProcessor(object):
    def __init__(self, errors, z_offset, localization_mode, include_distance):
        self.anchors = AnchorNodes(errors)
        self.anchors.add_anchors_from_file("Smart/Localization/anchors.txt")

        self.position_getter = PositionGetter(self.anchors, include_distance)

        self.x_state = None
        self.y_state = None
        self.z_state = None

        self.errors = errors

        self.z_offset = z_offset
        self.localization_mode = localization_mode
        self.include_distance = include_distance

    def reset(self):
        pass

    def get_position(self, distances_dict):
        success = self.anchors.set_all_distances(distances_dict)

        if success:
            # print("Successful")
            if self.include_distance:
                [position, distances_dict] = self.position_getter.get_position()
            else:
                position = self.position_getter.get_position()

            position[2] = self.z_offset - position[2]
            if self.x_state == None: # If position obtained is the first position obtained, make it initial value used in Kalman filter.
                self.x_state = KalmanState(q = 0.1, r = 1, p = 0.1, initial_val = position[0])
                self.y_state = KalmanState(q = 0.1, r = 1, p = 0.1, initial_val = position[1])
                self.z_state = KalmanState(q = 0.1, r = 1, p = 0.1, initial_val = position[2])
            else: # Otherwise, just update the measurements.
                self.x_state.update(measurement = position[0])
                self.y_state.update(measurement = position[1])
                self.z_state.update(measurement = position[2])

            result = [self.x_state.filtered_val, self.y_state.filtered_val, self.z_state.filtered_val]

            if self.localization_mode == GET_LSM:
                if self.include_distance:
                    return [position, distances_dict]
                else:
                    return position
            elif self.localization_mode == GET_KALMAN:
                if self.include_distance:
                    return [result, distances_dict]
                else:
                    return result
            else:
                if self.include_distance:
                    return [position, result, distances_dict]
                else:
                    return [position, result]
        else: # if distance acquisition is unsuccessful
            # self.errors.put(INSUFFICIENT_DISTANCES)
            return None
