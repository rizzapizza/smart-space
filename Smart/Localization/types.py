from collections import OrderedDict

from Smart.errors import NON_UNIQUE_ANCHOR_NODE, NON_EXISTENT_ANCHOR_NODE, INSUFFICIENT_DISTANCES

MINIMUM_NODE_COUNT = 3
MAX_NEG_ONE_COUNT = 3
REJECT_DISTANCE = 600

class AnchorNode(object):
    def __init__(self, name, x, y, z):
        self.name = name

        self.x = x
        self.y = y
        self.z = z

        self.distance = -1
        self.invalid_count = 0 # This represents the number of times a -1 has been received.

        self.alive = True

    def get_vector(self):
        return [self.x, self.y, self.z]

class AnchorNodes(object):
    def __init__(self, errors):
        self.dict = OrderedDict()
        self.errors = errors

        self.dead_anchor_count = 0
        self.anchor_count = 0

    def add_anchor(self, anchor_node):
        if anchor_node.name in self.dict:
            self.errors.put(NON_UNIQUE_ANCHOR_NODE)
        self.dict[anchor_node.name] = anchor_node
        self.anchor_count += 1

    def add_anchors_from_file(self, filename):
        f = open(filename, 'r')
        for line in f.readlines():
            values = line.split()
            new_node = AnchorNode(int(values[0]), float(values[1]), float(values[2]), float(values[3]))
            self.add_anchor(new_node)
        f.close()

    def get_vector_of_anchor(self, anchor_name):
        if not(anchor_name in self.dict):
            self.errors.put(NON_EXISTENT_ANCHOR_NODE)
        return self.dict[anchor_name].get_vector()

    def get_distance_of_anchor(self, anchor_name):
        if not(anchor_name in self.dict):
            self.errors.put(NON_EXISTENT_ANCHOR_NODE)
        return self.dict[anchor_name].distance

    def get_node_names(self):
        return self.dict.keys()

    def get_invalid_count_of_anchor(self, anchor_name):
        if not(anchor_name in self.dict):
            self.errors.put(NON_EXISTENT_ANCHOR_NODE)
        return self.dict[anchor_name].invalid_count

    def set_distance_of_anchor(self, anchor_name, distance):
        if not(anchor_name in self.dict):
            self.errors.put(NON_EXISTENT_ANCHOR_NODE)

        if distance < 0 or distance > REJECT_DISTANCE: # invalid distance
            # print("Got a -1 distance from Anchor " + str(anchor_name))
            self.dict[anchor_name].invalid_count += 1
            if (self.dict[anchor_name].alive and self.dict[anchor_name].invalid_count > MAX_NEG_ONE_COUNT):
                self.dict[anchor_name].alive = False
                self.dead_anchor_count += 1
            return False
        else:
            # print("Received distance " + str(distance) + " cm from anchor " + str(anchor_name))
            self.dict[anchor_name].invalid_count = 0
            self.dict[anchor_name].distance = distance
            return True

    def set_all_distances(self, distances_dict):
        if (self.anchor_count - self.dead_anchor_count) < MINIMUM_NODE_COUNT:
            self.errors.put(INSUFFICIENT_DISTANCES)

        node_count = 0

        for key in distances_dict:
            node_name = int(key)
            success = self.set_distance_of_anchor(node_name, distances_dict[node_name])
            if success:
                node_count += 1

        if node_count >= MINIMUM_NODE_COUNT: # If the number of nodes with less than 3 consecutive -1 distances is greater than or equal to MINIMUM_NODE_COUNT
            return True
        else:
            return False

    def get_all_distances(self): # returns a distance dictionary
        distances_dict = OrderedDict()
        for key in self.dict:
            distances_dict[key] = self.dict[key].distance
        return distances_dict
