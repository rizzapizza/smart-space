from __future__ import print_function
from lmfit import minimize, Parameters

import numpy as np

REJECT_DISTANCE = 600

class PositionGetter(object):
    def __init__(self, anchors, include_distance):
        self.anchors = anchors

        self.params = Parameters()
        names = ['solutionX', 'solutionY', 'solutionZ']
        for i in range(0, len(names)):
            self.params.add(names[i], value = 0)

        self.position = np.array((0, 0, 0))
        self.include_distance = include_distance

    def __residualfunct(self, params):
        X = params['solutionX'].value
        Y = params['solutionY'].value
        Z = params['solutionZ'].value

        result = []
        for name in self.anchors.get_node_names():
            distance = self.anchors.get_distance_of_anchor(name)
            if distance >= 0 and distance < REJECT_DISTANCE: # will cause an error if more than at least 2 distances are rejected
                coordinate_vector = self.anchors.get_vector_of_anchor(name)
                result.append(distance - np.linalg.norm(np.array((X, Y, Z)) - np.array(coordinate_vector)))
            #print("X: %f Y: %f Z: %f" % (X, Y, Z))

        self.position = np.array((X, Y, Z))
        return result

    def get_position(self):
        position = minimize(self.__residualfunct, self.params, args = ())
        if self.include_distance:
            return [self.position, self.anchors.get_all_distances()]
        return self.position
