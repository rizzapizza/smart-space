class CSVWriter(object):
    def __init__(self, filename):
        self.filewriter = open(filename, "w")

    def writeRow(self, values):
        str_to_write = ""

        for i in range(0, len(values)):
            str_to_write += str(values[i])
            if i != (len(values) - 1):
                str_to_write += ","

        str_to_write += "\n"
        self.filewriter.write(str_to_write)

    def close(self):
        self.filewriter.close()
