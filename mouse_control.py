import msvcrt

from Smart.Gesture.types import Gesture
from Smart.controller import Controller, ENABLE_ALL, MODE_LISTEN
from Smart.listener import Listener

import pyautogui

ARM_LENGTH = 65 # in cm

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

class MouseControlListener(Listener):
    def __init__(self, ref_length):
        print("Initialized.")

        self.x = 0
        self.y = 0

        x_size, y_size = pyautogui.size()
        self.x_conv_factor = x_size / ref_length
        self.y_conv_factor = y_size / ref_length

    def _move_cursor(self, measured_x, measured_y):
        x_movement = -(self.x - measured_x) * self.x_conv_factor
        y_movement = (self.y - measured_y) * self.y_conv_factor

        try:
            pyautogui.moveRel(x_movement, y_movement)
        except:
            print("Boundary reached.")

        self.x = measured_x
        self.y = measured_y

    def on_gesture(self, gesture):
        if gesture == Gesture.Down:
            pyautogui.click()

    def on_location(self, location):
        self._move_cursor(location[1], location[2])

def main():
    controller = Controller(ENABLE_ALL, MODE_LISTEN)
    listener = MouseControlListener(ARM_LENGTH)
    controller.add_listener(listener)
    controller.start()

    print("Press CTRL + ALT + Q to exit.")
    while True:
        try:
            x = kbfunc()

            if x == 16:
                raise KeyboardInterrupt
            elif not(controller.errors.empty()):
                print("One or more errors have occured.")
                while not(controller.errors.empty()):
                    print("\t" + str(controller.errors.get()))
                raise Exception("An error has occurred.")
        except:
            controller.stop()
            break

if __name__ == "__main__":
    main()
