import msvcrt

from Smart.controller import Controller, ENABLE_ALL, MODE_LISTEN
from Smart.listener import Listener

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

class TestListener(Listener):
    def __init__(self):
        print("Initialized.")

    def on_gesture(self, gesture):
        print("Gesture: " + str(gesture))

    def on_location(self, location):
        print("Location: %f %f %f" % (location[0], location[1], location[2]))

    def on_acceleration(self, data):
        print("Acceleration at time %d: %f %f" % (data[0], data[1], data[2]))

def main():
    controller = Controller(ENABLE_ALL, MODE_LISTEN)
    listener = TestListener()
    controller.add_listener(listener)
    controller.start()

    print("Press CTRL + ALT + Q to exit.")
    while True:
        try:
            x = kbfunc()

            if x == 16:
                raise KeyboardInterrupt
            elif not(controller.errors.empty()):
                print("\nOne or more errors have occured.")
                print("\t" + str(controller.errors.get()))
                raise Exception("An error has occurred.")
        except:
            controller.stop()
            break

if __name__ == "__main__":
    main()
