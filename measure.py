from Smart.controller import Controller, ENABLE_LOCALIZATION, MODE_LISTEN, GET_BOTH
from Smart.listener import Listener
from Smart.csv_writer import CSVWriter

from Queue import Queue

FILENAME = "results5.csv"

SALAZAR = 0
GODRIC = 1
ROWENA = 2
HELGA = 3

class MyListener(Listener):
    def __init__(self, data_queue, go_queue, start_queue):
        self.data_queue = data_queue
        self.go_queue = go_queue
        self.start_queue = start_queue

    def on_location(self, location):
        if self.start_queue.empty():
            self.start_queue.put(1) # Tell program below to start when done with controller initialization
        if self.go_queue.full():
            self.data_queue.put(location)

def main():
    controller = Controller(ENABLE_LOCALIZATION, MODE_LISTEN, localization_mode = GET_BOTH, include_distance = True)

    data_queue = Queue()
    go_queue = Queue(1)
    start_queue = Queue(1)

    listener = MyListener(data_queue, go_queue, start_queue)
    controller.add_listener(listener)

    controller.start()

    writer = CSVWriter(FILENAME)
    writer.writeRow(["True x", "True y", "True z", "Measured x (LSM)", "Measured y (LSM)", "Measured z (LSM)", "Measured x (Kalman)", "Measured y (Kalman)", "Measured z (Kalman)", "Distance from Salazar", "Distance from Godric", "Distance from Rowena", "Distance from Helga"])

    while True:
        if not(controller.errors.empty()):
            print("One or more errors have occurred.")
            while not(controller.errors.empty()):
                print("\t" + str(controller.errors.get()))
            break
        elif start_queue.full():
            invalid = False
            true_coordinates = raw_input("\nInput the true coordinates separated by commas (i.e., '0,0,0'): ")
            try:
                no_of_samples = input("Input the number of samples: ")
            except:
                invalid = True

            true_coordinates = true_coordinates.split(",")
            if len(true_coordinates) == 3:
                try:
                    for i in range(0, len(true_coordinates)):
                        true_coordinates[i] = float(true_coordinates[i])
                except:
                    invalid = True
            else:
                invalid = True

            if not(invalid):
                count = 0
                error_occured = False
                while not(data_queue.empty()): # empty data queue so no data corresponding to previous true coordinates will be obtained
                    data_queue.get()

                go_queue.put(1) # signal listener to start taking positions
                print("\nGetting samples for point with coordinates " + str(true_coordinates))
                while count < no_of_samples:
                    if not(controller.errors.empty()):
                        error_occured = True
                        break
                    if not(data_queue.empty()):
                        pos = data_queue.get()

                        lsm_pos = pos[0]
                        kalman_pos = pos[1]
                        distances_dict = pos[2]

                        print("\nObtained position from LSM: " + str(lsm_pos))
                        print("Obtained position from Kalman Filter: " + str(kalman_pos))
                        print("Obtained distances: " + str(distances_dict))

                        writer.writeRow([true_coordinates[0], true_coordinates[1], true_coordinates[2], lsm_pos[0], lsm_pos[1], lsm_pos[2], kalman_pos[0], kalman_pos[1], kalman_pos[2], distances_dict[SALAZAR], distances_dict[GODRIC], distances_dict[ROWENA], distances_dict[HELGA]])

                        print("Wrote new data to file.")
                        count += 1

                go_queue.get()

                if not(error_occured):
                    ans = raw_input("\nDone? (Press y for 'yes' and any other key for no): ")
                    if ans == "y" or ans == "Y":
                        break
            else:
                print("\nInvalid inputs.")
                ans = raw_input("Try again? (Press y for 'yes' and any other key for no): ")
                if ans == "y" or ans == "Y":
                    break

    writer.close()
    print("\nFinished writing to file.")
    controller.stop()
    print("Program closed.")

if __name__ == "__main__":
    main()
