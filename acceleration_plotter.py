from Smart.controller import Controller, ENABLE_GESTURE, MODE_LISTEN
from Smart.listener import Listener

import matplotlib.pyplot as plt
import matplotlib.animation as animation

from Queue import Queue
from threading import Thread

DEFAULT_DATA_LIMIT = 100

class AccelerometerDataManager(object):
    def __init__(self, data_queue, data_limit = DEFAULT_DATA_LIMIT):
        self.data_queue = data_queue

        if data_limit > 0:
            self.data_limit = data_limit
        else:
            self.data_limit = DEFAULT_DATA_LIMIT

        self.ms_passed = []

        self.x_values = []
        self.y_values = []

    def get_value_lists(self):
        count = 0
        while not(self.data_queue.empty()) and count < self.data_limit: # while queue is not empty and while the entire list has not been completely replaced
            if len(self.ms_passed) == self.data_limit:
                self.ms_passed.pop(0)

                self.x_values.pop(0)
                self.y_values.pop(0)

            data = self.data_queue.get()

            self.ms_passed.append(data[0])

            self.x_values.append(data[1])
            self.y_values.append(data[2])

            count += 1

        return [self.ms_passed, self.x_values, self.y_values]

class AccelerationListener(Listener):
    def __init__(self, plot_q):
        self.plot_q = plot_q

    def on_acceleration(self, acc):
        self.plot_q.put(acc)
        print(acc)

    def on_gesture(self, gesture):
        print("Gesture " + str(gesture) + " detected.")

def animate(arg, subplots, y_labels, accelerometer_data_manager):
	value_lists = accelerometer_data_manager.get_value_lists()

	for i in range(0, 2):
		subplots[i].clear()
		subplots[i].set_title(y_labels[i])
		subplots[i].set_xticks([])
		subplots[i].plot(value_lists[0], value_lists[i + 1])

def main():
    plot_q = Queue() # queue to contain plot data
    accelerometer_data_manager = AccelerometerDataManager(plot_q)

    # prepare plots
    fig = plt.figure()

    main_subplot = fig.add_subplot(1, 1, 1)
    main_subplot.set_xlabel("Time")
    main_subplot.set_xticks([])
    main_subplot.set_yticks([])

    y_labels = ["X Acceleration (m/s^2)", "Y Acceleration (m/s^2)"]
    subplots = []

    for i in range(1, 3):
        subplot = fig.add_subplot(2, 1, i)
        subplot.set_title(y_labels[i - 1])
        subplot.set_xticks([])
        subplots.append(subplot)

    # plot animation function preparation
    ani = animation.FuncAnimation(fig, animate, interval = 20, fargs = (subplots, y_labels, accelerometer_data_manager))
    # prepare controller and listener
    controller = Controller(ENABLE_GESTURE, MODE_LISTEN)
    listener = AccelerationListener(plot_q)
    controller.add_listener(listener)
    controller.start()

    if not(controller.errors.empty()):
        print("\nOne or more errors have occured.")
        print("\t" + str(controller.errors.get()))
    else:
        plt.show()
        print("Plot shown")
        controller.stop()

if __name__ == "__main__":
    main()
