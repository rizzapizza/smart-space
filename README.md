# README #

This README contains steps which are necessary to get the system up and running and the SDK ready for use in Python applications.

### What is this repository for? ###

* This repository serves as an SDK for Smart Space: a system which uses three-dimensional localization and gesture recognition for human-computer interaction. It is written in Python and can only be imported in Python applications.

### How do I get set up? ###

The system is sure to work on a Windows 64-bit computer using Python 2.7. The instructions below show the complete instructions on how to setup the system.

* First, make sure that you have all the necessary hardware for the system: 4 anchor nodes, 1 gateway node, and 1 mobile node (a wearable device)
* Next, install Python. This system uses Python 2.7.
* Install easy_install. On windows, you can do this by downloading ez_setup.py from [here](https://bootstrap.pypa.io/ez_setup.py) and running it with python. Type the following command on the Windows CMD: `python ez_setup.py`
* Next, get pip using this command: `easy_install pip`
* Now, install virtualenv. Type in this command: `pip install virtalenv`
* Create a directory which will serve as your workspace. Enter that directory on the command prompt.
* Now, type the following command: `virtualenv venv`
* You now have a running virtual environment for the project. Now, activate the virtual environment with: `venv\Scripts\activate.bat`
* Clone this repository in the same directory.
* Change your current directory to the newly created smart-space directory.
* Type the following command: `pip install -r requirements.txt`
* The previous command installs all Python packages in requirements.txt. If one or more packages fail to install, you might have to install it manually. This [site]() contains a good list of Python packages ready for installation on Windows.
* If you need to change the coordinates of the anchor nodes, go to smart-space/Smart/Localization and modify anchors.txt. 
* Turn on all devices and connect the gateway node to the computer.

### Usage ###

There are two ways to acquire system data using the SDK. One is through polling and another is through the use of the observer pattern. For both cases, one needs to create what is known as a controller. The controller starts the system and allows for the acquisition of both localization and gesture data.

#### How to Use the Controller ####

First, make sure you import the following:

```Python
from Smart.controller import Controller
```

All constants mentioned in the description of each parameter can also be imported from `Smart.controller`

```Python
from Smart.controller import ENABLE_LOCALIZATION, ENABLE_GESTURE, ENABLE_ALL`
from Smart.controller import MODE_LISTEN, MODE_POLL`
from Smart.controller import GET_KALMAN, GET_LSM, GET_BOTH
```

Now, create a controller.

```Python
controller = Controller(enable = ENABLE_GESTURE, mode = MODE_LISTEN)
```

You can specify several parameters in the controller constructor.

1. enable - This can be set to `ENABLE_GESTURE`, `ENABLE_LOCALIZATION` or `ENABLE_ALL` depending on the needs of your application. It is a required parameter.
2. mode - This can be set to `MODE_POLL` or `MODE_LISTEN` based on which method you wish to use for data acquisition. It is a required parameter.
3. listen_for_localization_none - This can be set to `True` or `False` depending on whether you wish to be notified if invalid coordinates have been obtained or not. By default, the value of this parameter is `False`.
4. listen_for_gesture_none - This can be set to `True` or `False` depending on whether you wish to be notified when no gestures have been recognized or not. By default, the value of this parameter is `False`.
5. verbose - This can be set to `True` or `False` depending on whether you wish to display debug messages or not. By default, the value of this parameter is `False`.
6. acceleration_filename - When this is not empty, acceleration data obtained during a system run are written to a file with this name. By default, the value of this parameter is `None`.
7. localization_mode - This can be set to `GET_KALMAN`, `GET_LSM` or `GET_BOTH`. If its value is `GET_KALMAN`, then the locations returned by the controller are the outputs of the Kalman filter. If its value is `GET_LSM`, then the locations returned by the controller are the outputs of the LSM. If its value is `GET_BOTH`, both locations for both modes are obtained. The default value of this parameter is `GET_KALMAN`.
8. include_distance - This can be set to `True` or `False` depending on whether you wish the controller to include the distances to each anchor node when it returns obtained positions or not. By default, the value of this parameter is `False`.

Next, initialize the system through the following code:

```Python
controller.start()
```

Finally, make sure that the system and all created threads are stopped through the following code:

```Python
controller.stop()
```

#### Data Acquisition ####

##### Polling #####

An example piece of code which tells the controller to poll for acquired data is written below.

```Python
controller = Controller(ENABLE_GESTURE, MODE_POLL)
controller.start()

while True:
    gesture = controller.get_gesture()
```

##### Observer Pattern #####

In order to make use of the observer pattern, a new listener class which inherits from the original listener class in the SDK needs to be created. 

```Python
from Smart.listener import Listener

class MyListener(Listener):
    def on_location(location):
        pass

    def on_acceleration(acceleration):
        pass

    def on_gesture(gesture):
        pass
```

Any of the three methods above (`on_location`, `on_acceleration` and `on_gesture` can be overriden)

The controller is set up in the following manner:

```Python
controller = Controller(ENABLE_GESTURE, MODE_POLL)
listener = MyListener()
controller.add_listener(listener)
controller.start()
```

#### Error Checking ####

Checking for errors involves writing the following code:

```Python
if not(controller.errors.empty()):
    print("One or more errors have occured.")
    while not(controller.errors.empty()):
        print("\t" + str(controller.errors.get()))
        raise Exception("An error has occurred.")
```

### Example Applications ###

Several example applications have been made in the repository. These are:

1. `acceleration_plotter.py` - When run, this application plots the acceleration data received from the mobile device.
2. `example_listen.py` - When run, this application uses the observer pattern to acquire localization, gesture and acceleration data and simply prints them. 
3. `example_poll.py` - When run, this application polls for new gesture data and prints the names of recgonized gestures.
4. `gui_server.py` - This is run before the Unity Localization GUI can be ran.
5. `mouse_control.py` - This controls the mouse pointer through PyAutoGUI.
6. `scroll.py` and `scroll_no_poll.py` - This enables window scrolling through PyAutoGUI. The two versions use polling and the observer pattern, respectively.
7. `slide_control.py` and `slide_control_no_poll.py` - This enables slide control through PyAutoGUI. The two versions use polling and the observer pattern, respectively.
8. `unity_server_restart.py` - This application is run before the interactive room game (Serious Game version 4)
9. Others - Other applications were used in trials and data measurements for documentation writing.

### Who do I talk to? ###

* If you have any questions or clarifications, please email: smartspace.ucl@gmail.com.