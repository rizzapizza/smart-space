from Smart.controller import Controller, ENABLE_LOCALIZATION, MODE_LISTEN, GET_BOTH
from Smart.listener import Listener
from Smart.csv_writer import CSVWriter

from Queue import Queue

FILENAME = "results5.csv"

SALAZAR = 0
GODRIC = 1
ROWENA = 2
HELGA = 3

class MyListener(Listener):
    def __init__(self, data_queue, go_queue, start_queue):
        self.data_queue = data_queue
        self.go_queue = go_queue
        self.start_queue = start_queue

    def on_location(self, location):
        if self.start_queue.empty():
            self.start_queue.put(1) # Tell program below to start when done with controller initialization
        if self.go_queue.full():
            self.data_queue.put(location)

def main():
    controller = Controller(ENABLE_LOCALIZATION, MODE_LISTEN, localization_mode = GET_BOTH, include_distance = True)

    data_queue = Queue()
    go_queue = Queue(1)
    start_queue = Queue(1)

    listener = MyListener(data_queue, go_queue, start_queue)
    controller.add_listener(listener)

    controller.start()

    while start_queue.empty(): # while controller is not finished initializing
        pass

    if not(controller.errors.empty()):
        print("One or more errors have occurred.")
        while not(controller.errors.empty()):
            print("\t" + str(controller.errors.get()))
    else:
        f = open("measure_file_input.txt", "r")

        first_line = True
        valid_input_file = True

        no_of_samples = 0
        true_coordinates = []

        for line in f.readlines():
            if first_line:
                try:
                    no_of_samples = int(line)
                    first_line = False
                except:
                    valid_input_file = False
                    break
            else:
                values = line.split(" ")
                if len(values) != 3:
                    valid_input_file = False
                    break
                else:
                    try:
                        true_coordinates.append([])
                        for i in range(0, len(values)):
                            true_coordinates[-1].append(float(values[i]))
                    except:
                        valid_input_file = False

        f.close()

        if valid_input_file:
            print("Finished reading input file.")

            writer = CSVWriter(FILENAME)
            writer.writeRow(["True x", "True y", "True z", "Measured x (LSM)", "Measured y (LSM)", "Measured z (LSM)", "Measured x (Kalman)", "Measured y (Kalman)", "Measured z (Kalman)", "Distance from Salazar", "Distance from Godric", "Distance from Rowena", "Distance from Helga"])

            for coordinate in true_coordinates:
                print("\nGetting samples for point with coordinates (%f, %f, %f)..." % (coordinate[0], coordinate[1], coordinate[2]))
                raw_input("Press ENTER to begin.")

                count = 0
                error_occured = False
                while not(data_queue.empty()): # empty data queue so no data corresponding to previous true coordinates will be obtained
                    data_queue.get()

                go_queue.put(1) # signal listener to start taking positions

                while count < no_of_samples:
                    if not(controller.errors.empty()):
                        error_occured = True
                        break
                    if not(data_queue.empty()):
                        pos = data_queue.get()

                        lsm_pos = pos[0]
                        kalman_pos = pos[1]
                        distances_dict = pos[2]

                        print("\nObtained position from LSM: " + str(lsm_pos))
                        print("Obtained position from Kalman Filter: " + str(kalman_pos))
                        print("Obtained distances: " + str(distances_dict))

                        writer.writeRow([coordinate[0], coordinate[1], coordinate[2], lsm_pos[0], lsm_pos[1], lsm_pos[2], kalman_pos[0], kalman_pos[1], kalman_pos[2], distances_dict[SALAZAR], distances_dict[GODRIC], distances_dict[ROWENA], distances_dict[HELGA]])

                        print("Wrote new data to file.")
                        count += 1

                go_queue.get() # signal listener to stop putting positions in the data queue
                if error_occured:
                    print("One or more errors have occurred.")
                    while not(controller.errors.empty()):
                        print("\t" + str(controller.errors.get()))
                    break

            writer.close()
        else:
            print("Invalid input file.")

    controller.stop()
    print("Program closed.")

if __name__ == "__main__":
    main()
