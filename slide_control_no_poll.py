from Smart.Gesture.gesture_recognizer import Gesture
from Smart.controller import Controller, ENABLE_GESTURE, MODE_LISTEN
from Smart.listener import Listener

import pyautogui, msvcrt

class GestureListener(Listener):
    def on_gesture(self, gesture):
        print(str(gesture))
        if gesture == Gesture.Left:
            pyautogui.press('left')
        elif gesture == Gesture.Right:
            pyautogui.press('right')
        elif gesture == Gesture.Up:
            pyautogui.press('up')
        elif gesture == Gesture.Down:
            pyautogui.press('down')

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

def main():
    controller = Controller(ENABLE_GESTURE, MODE_LISTEN, listen_for_gesture_none = True, acceleration_filename = "acceleration")
    listener = GestureListener()

    controller.add_listener(listener)
    controller.start()
    print("Press CTRL + ALT + Q to exit.")
    while True:
        try:
            x = kbfunc()

            if x == 16:
                raise KeyboardInterrupt
            elif not(controller.errors.empty()):
                print("One or more errors have occured.")
                while not(controller.errors.empty()):
                    print("\t" + str(controller.errors.get()))
                raise Exception("An error has occurred.")
        except:
            print("Exception occurred.")
            controller.stop()
            break

if __name__ == "__main__":
    main()
