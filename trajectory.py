import msvcrt

from Smart.controller import Controller, ENABLE_LOCALIZATION, MODE_LISTEN, GET_BOTH
from Smart.listener import Listener
from Smart.csv_writer import CSVWriter

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

class TestListener(Listener): # writes locations to file
    def __init__(self, filename):
        self.lsm_filehandler = CSVWriter("Trajectory Tests/" + filename + "_lsm.csv")
        self.kalman_filehandler = CSVWriter("Trajectory Tests/" + filename + "_kalman.csv")

        self.lsm_filehandler.writeRow(["X", "Y", "Z"])
        self.kalman_filehandler.writeRow(["X", "Y", "Z"])

        print("Initialized.")

    def on_location(self, value):
        lsm_location = value[0]
        kalman_location = value[1]
        distances = value[2]

        print("Location: %f %f %f" % (kalman_location[0], kalman_location[1], kalman_location[2]))
        print("Distances: " + str(distances))

        self.lsm_filehandler.writeRow([lsm_location[0], lsm_location[1], lsm_location[2]])
        self.kalman_filehandler.writeRow([kalman_location[0], kalman_location[1], kalman_location[2]])

    def close(self):
        self.lsm_filehandler.close()
        self.kalman_filehandler.close()

def main():
    filename = "bounce_test"

    controller = Controller(ENABLE_LOCALIZATION, MODE_LISTEN, localization_mode = GET_BOTH, include_distance = True)
    listener = TestListener(filename)
    controller.add_listener(listener)
    controller.start()

    print("Press CTRL + ALT + Q to exit.")
    while True:
        try:
            x = kbfunc()

            if x == 16:
                raise KeyboardInterrupt
            elif not(controller.errors.empty()):
                print("One or more errors have occured.")
                while not(controller.errors.empty()):
                    print("\t" + str(controller.errors.get()))
                raise Exception("An error has occurred.")
        except:
            controller.stop()
            listener.close()
            break

if __name__ == "__main__":
    main()
