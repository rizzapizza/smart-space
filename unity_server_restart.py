'''This server handles restarts.'''

import socket
import time
import msvcrt

from Smart.listener import Listener
from Smart.controller import Controller, ENABLE_ALL, MODE_LISTEN

from threading import Thread
from Queue import Queue

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

def run_server(connection_queue, connected_queue, stop_queue):
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    serversocket.bind(('localhost', 9999))

    connection = None

    while stop_queue.empty():
        if connected_queue.empty():
            serversocket.listen(1)
            print("Looking for client...")
            connection, address = serversocket.accept()

            connection_queue.put(connection)
            connected_queue.put(1) # queue to indicate if server is currenty connected to a client

            print("Client connected.")
        else:
            if connection != None:
                msg = connection.recv(4) # wait for stop
                if msg == "STOP": # game was closed and not restarted
                    break
                connection.close()
                connection = None

    serversocket.close()

class UnityListener(Listener):
    def __init__(self, connection_queue, connected_queue):
        self.connection = None

        self.connection_queue = connection_queue
        self.connected_queue = connected_queue

        self.last_location = None
        self.last_time = None

    def try_connect(self):
        if self.connection_queue.empty():
            return False # nothing to connect to
        else:
            self.connection = self.connection_queue.get()

    def connected(self):
        return (self.connection != None)

    def disconnect(self):
        self.connection = None

        self.connected_queue.get()

    def _send(self, state, location, gesture):
        if location != None:
            msg = "%s %f %f %f %s\n" % (state, location[0], location[1], location[2], str(gesture))
        else:
            msg = "%s %s %s\n" % (state, str(location), str(gesture))

        try:
            self.connection.send(msg)
            print("Sent " + str(msg))
        except:
            self.disconnect()
            print("Client disconnected.")

    def on_gesture(self, gesture):
        print("Gesture found.")
        if self.connected() or self.try_connect():
            self._send("G", self.last_location, gesture)
        else:
            print("Gesture not sent.")

    def on_location(self, location):
        print("Location found.")
        if self.connected() or self.try_connect():
            curr_time = int(round(time.time() * 1000))
            if self.last_time != None:
                print("Time since last: " + str(curr_time - self.last_time))
            self.last_time = curr_time

            if location != None:
                self._send("L", location[0], None)
                print("Distances: " + str(location[1]))

                self.last_location = location[0]
            else:
                self._send("L", None, None)
        else:
            print("Location not sent.")

controller = Controller(ENABLE_ALL, MODE_LISTEN, listen_for_localization_none = True, listen_for_gesture_none = True, include_distance = True)

connection_queue = Queue(1)
connected_queue = Queue(1)
stop_queue = Queue(1)

server_thread = Thread(target = run_server, args = (connection_queue, connected_queue, stop_queue))
server_thread.start()

listener = UnityListener(connection_queue, connected_queue)
controller.add_listener(listener)
controller.start()

print("Press CTRL + ALT + Q to quit.")

while True:
    try:
        x = kbfunc()

        if x == 16:
            raise KeyboardInterrupt
        elif not(controller.errors.empty()):
            print("One or more errors have occured.")
            while not(controller.errors.empty()):
                print("\t" + str(controller.errors.get()))
            raise Exception("An error has occurred.")
    except:
        controller.stop()

        stop_queue.put(1)
        server_thread.join()
        break
