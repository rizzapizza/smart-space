from Smart.Gesture.gesture_recognizer import Gesture
from Smart.controller import Controller, ENABLE_GESTURE, MODE_LISTEN
from Smart.listener import Listener

import pyautogui

import msvcrt

class GestureToScrollConverter(object):
    def __init__(self, scroll_amount = 50):
        self.scroll_amount = scroll_amount

    def convert(self, gesture):
        if gesture == Gesture.Up:
            pyautogui.scroll(self.scroll_amount)
            print("Scrolled up.")
        elif gesture == Gesture.Down:
            print("Scrolled down.")
            pyautogui.scroll(-self.scroll_amount)

class GestureListener(Listener):
    def __init__(self):
        self.converter = GestureToScrollConverter()

    def on_gesture(self, gesture):
        print(str(gesture))
        self.converter.convert(gesture)

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

def main():
    controller = Controller(ENABLE_GESTURE, MODE_LISTEN, listen_for_gesture_none = True)
    listener = GestureListener()

    controller.add_listener(listener)
    controller.start()
    print("Press CTRL + ALT + Q to exit.")
    while True:
        try:
            x = kbfunc()

            if x == 16:
                raise KeyboardInterrupt
            elif not(controller.errors.empty()):
                print("One or more errors have occured.")
                while not(controller.errors.empty()):
                    print("\t" + str(controller.errors.get()))
                raise Exception("An error has occurred.")
        except:
            controller.stop()
            break

if __name__ == "__main__":
    main()
