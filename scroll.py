from Smart.Gesture.gesture_recognizer import Gesture
from Smart.controller import Controller, ENABLE_GESTURE, MODE_POLL

import pyautogui

import msvcrt

class GestureToScrollConverter(object):
    def __init__(self, scroll_amount = 10):
        self.scroll_amount = scroll_amount

    def convert(self, gesture):
        if gesture == Gesture.Up:
            pyautogui.scroll(self.scroll_amount)
        elif gesture == Gesture.Down:
            pyautogui.scroll(-self.scroll_amount)

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

def main():
    converter = GestureToScrollConverter()

    controller = Controller(ENABLE_GESTURE, MODE_POLL)
    controller.start()
    print("Press CTRL + ALT + Q to exit.")
    while True:
        try:
            x = kbfunc()

            gesture = controller.get_gesture()
            if gesture != None:
                print(str(gesture))
                converter.convert(gesture)
            if x == 16:
                raise KeyboardInterrupt
            elif not(controller.errors.empty()):
                print("One or more errors have occured.")
                while not(controller.errors.empty()):
                    print("\t" + str(controller.errors.get()))
                raise Exception("An error has occurred.")
        except:
            controller.stop()
            break

if __name__ == "__main__":
    main()
