import socket
import time
import msvcrt

from Smart.listener import Listener
from Smart.controller import Controller, ENABLE_LOCALIZATION, MODE_LISTEN

def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

class UnityListener(Listener):
    def __init__(self, connection):
        self.connection = connection

    def _send(self, state, location):
        msg = "%f %f %f\n" % (location[0], location[1], location[2])

        try:
            self.connection.send(msg)
            print("Sent " + str(msg))
        except:
            self.connection.close()
            print("Client disconnected.")

    def on_location(self, location):
        print("Location found.")
        if location != None:
            self._send("L", location[0])
            print("Distances: " + str(location[1]))

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(('localhost', 9999))
serversocket.listen(1) # become a server socket, maximum 5 connections

connection, address = serversocket.accept()
print("Client connected.")

controller = Controller(ENABLE_LOCALIZATION, MODE_LISTEN, listen_for_localization_none = True, listen_for_gesture_none = True, include_distance = True)
listener = UnityListener(connection)

controller.add_listener(listener)
controller.start()

while True:
    try:
        x = kbfunc()

        if x == 16:
            raise KeyboardInterrupt
        elif not(controller.errors.empty()):
            print("One or more errors have occured.")
            while not(controller.errors.empty()):
                print("\t" + str(controller.errors.get()))
            raise Exception("An error has occurred.")
    except:
        controller.stop()
        break
